
function balEnq() {
    Lungo.Notification.show();
    var url = "http://executive.smstopupng.com/api.aspx";
    var userObject = Lungo.Cache.get("userData");
    var reqObj = {
        type: "cbal",
        username: userObject.username,
        password: userObject.password
    };
    var parseResponse = function(result) {
        Lungo.Notification.success("Success", result.slice(9), "check", 4);
        userObject.balance = result.replace(/[ A-Za-z$-]/g, "");
        Lungo.Cache.set("userData", userObject);
        $$('#mySideBarTitle').data('title', userObject.balance);
        $$('#mySideBarTitle h1.title').text('SMS BAL ' + userObject.balance);
    };

    Lungo.Service.Settings.async = true;
    Lungo.Service.Settings.error = function(type, xhr) {
        Lungo.Notification.error(
                "Server Error", //Title
                "An Internal Server Error has occured.Please try again", //Description
                "cancel", //Icon
                4);
        console.log("error" + type + JSON.stringify(xhr));
    };
    Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
    Lungo.Service.Settings.crossDomain = true;
    Lungo.Service.get(url, reqObj, parseResponse, "text");
}


function sendRegistration() {

    var userObject = Lungo.Cache.get("userData");
    var regObject = {
        type: 'sendmsg',
        message: $$('#genMsg_msg').val(),
        campaignName: $$('#genMsg_senderID').val(),
        isGrp: false,
        recipients: $$('#genMsg_recipients').val(),
        username: userObject.username,
        password: userObject.password
    };

    Lungo.Notification.show();
    var url = "http://executive.smstopupng.com/api.aspx";

    var parseResponse = function(result) {

        if (result.substring(0, 7) === "success") {
            Lungo.Notification.success("Success", "Message sent", "check", 3);
            cleanUp('form-normal');
            Lungo.Router.article("square-menu", "square-menu-items");
        }
        else {
            Lungo.Notification.error(
                    "Failed", //Title
                    result.slice(9), //Description
                    "cancel", //Icon
                    4);


        }
    };
    Lungo.Service.Settings.async = true;
    Lungo.Service.Settings.error = function(type, xhr) {
        Lungo.Notification.error(
                "Server Error", //Title
                "An Internal Server Error has occured.Please try again", //Description
                "cancel", //Icon
                4);
        console.log("error" + type + JSON.stringify(xhr));
    };
    Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
    Lungo.Service.Settings.crossDomain = true;
    Lungo.Service.get(url, regObject, parseResponse, "text");

}

function sendGrpMsgReq() {

    var userObject = Lungo.Cache.get("userData");
    var regObject = {
        type: 'sendgrp',
        message: $$('#grpMsg_msg').val(),
        campaignName: $$('#grpMsg_senderID').val(),
        recipients: $$('#grpMsg_recipient_hiddden').val(),
        username: userObject.username,
        password: userObject.password
    };
    console.log(regObject);
    Lungo.Notification.show();
    var url = "http://executive.smstopupng.com/api.aspx";

    var parseResponse = function(result) {

        if (result.substring(0, 7) === "success") {
            Lungo.Notification.success("Success", "Message sent", "check", 3);
            cleanUp('grp-msg-holder');
            Lungo.Router.article("square-menu", "square-menu-items");
        }
        else {
            Lungo.Notification.error(
                    "Failed", //Title
                    result.slice(9), //Description
                    "cancel", //Icon
                    4);


        }
    };
    Lungo.Service.Settings.async = true;
    Lungo.Service.Settings.error = function(type, xhr) {
        Lungo.Notification.error(
                "Server Error", //Title
                "An Internal Server Error has occured.Please try again", //Description
                "cancel", //Icon
                4);
        console.log("error" + type + JSON.stringify(xhr));
    };
    Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
    Lungo.Service.Settings.crossDomain = true;
    Lungo.Service.get(url, regObject, parseResponse, "text");

}

function sendTrnsfRequest(amount, recipient) {

    var userObject = Lungo.Cache.get("userData");
    var regObject = {
        type: 'transfer',
        destUsername: recipient,
        amt: amount,
        username: userObject.username,
        password: userObject.password
    };

    {
        Lungo.Notification.show();
        var url = "http://executive.smstopupng.com/api.aspx";

        var parseResponse = function(result) {

            if (result.substring(0, 7) === "success") {
                Lungo.Notification.success(
                        "Success", //Title
                        'Transfer Successful', //Description
                        "check", //Icon
                        4);
                cleanUp('transfer-sms-holder');
                Lungo.Router.article("square-menu", "square-menu-items");
            }
            else {
                Lungo.Notification.error("Error", result.slice(9), "cancel", 3);
            }
        };
        Lungo.Service.Settings.async = true;
        Lungo.Service.Settings.error = function(type, xhr) {
            Lungo.Notification.error(
                    "Server Error", //Title
                    "An Internal Server Error has occured.Please try again", //Description
                    "cancel", //Icon
                    4);
            console.log("error" + type + JSON.stringify(xhr));
        };
        Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
        Lungo.Service.Settings.crossDomain = true;
        Lungo.Service.get(url, regObject, parseResponse, "text");
    }
}


function sendRechargeRequest(pin) {

    var userObject = Lungo.Cache.get("userData");
    var regObject = {
        type: 'load',
        pin: pin,
        username: userObject.username,
        password: userObject.password
    };

    {
        Lungo.Notification.show();
        var url = "http://executive.smstopupng.com/api.aspx";

        var parseResponse = function(result) {

            if (result.substring(0, 7) === "success") {
                userObject.balance = result.replace(/[ A-Za-z$-]/g, "");
                Lungo.Cache.set("userData", userObject);
                Lungo.Notification.success(
                        "Success", //Title
                        'Recharge of ' + userObject.balance + ' units successful', //Description
                        "check", //Icon
                        4);
                cleanUp('recharge-holder');
                // Lungo.Router.article("square-menu", "square-menu-items");
            }
            else {
                Lungo.Notification.error("Error", result.slice(9), "cancel", 3);
            }
        };
        Lungo.Service.Settings.async = true;
        Lungo.Service.Settings.error = function(type, xhr) {
            Lungo.Notification.error(
                    "Server Error", //Title
                    "An Internal Server Error has occured.Please try again", //Description
                    "cancel", //Icon
                    4);
            console.log("error" + type + JSON.stringify(xhr));
        };
        Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
        Lungo.Service.Settings.crossDomain = true;
        Lungo.Service.get(url, regObject, parseResponse, "text");
    }
}


function fetchGrup() {

    Lungo.Notification.show();

    var url = "http://executive.smstopupng.com/api.aspx";
    var userObject = Lungo.Cache.get("userData");
    //console.log(JSON.stringify(userObject));
    var data = {type: "getgrp", username: userObject.username, password: userObject.password};
    var parseResponse = function(result) {
        var domoutput = "<table border='0'>";
        if (result !== "") {
            console.log(result);
            var grp = result.slice(result.search(';') + 1).replace(/;+$/, "");
            if (grp !== "") {
                grp = grp.split(";");
            }
            console.log(grp);

            if (grp.length > 0) {
                for (var i = 0; i < grp.length; i++) {
                    var temp = grp[i];
                    temp = temp.split(',');

                    domoutput += "<tr><td><input type='checkbox' value='" + temp[1] + "'></td><td><label style='padding-left:10px'> " + temp[1] + "</label></td></tr>";

                }
                domoutput += "</table>";
                document.getElementById('fetched_groups').innerHTML = (domoutput);
            }
            Lungo.Notification.hide();

        }
        else if (result.substring(0, 7) !== "failure") {
            Lungo.Notification.hide();
            Lungo.Notification.error(
                    "Failed", //Title
                    "Failed to fetch group list. Please try again later", //Description
                    "cancel", //Icon
                    4);
        }
        else {
            Lungo.Notification.hide();
            Lungo.Notification.error("", "Network connection problem. Please try again", "", 4);
        }

    };
    Lungo.Service.Settings.async = true;
    Lungo.Service.Settings.error = function(type, xhr) {
        Lungo.Notification.hide();
        console.log("error" + type + JSON.stringify(xhr));
        Lungo.Notification.error("Error", "An error occured in processing the server request. <br/>Please contact \n\
                            the server admin.", "cancel", 4);
    };
    Lungo.Service.Settings.headers["Content-Type"] = "text/plain";
    Lungo.Service.Settings.crossDomain = true;
    //Lungo.Service.Settings.timeout = 30;
    Lungo.Service.get(url, data, parseResponse, "text");
}


function gotoformpg2() {

    var genMsg_Msg = $$("#genMsg_msg").val();
    var genMsg_msgCounter = $$("#genMsg_msgCounter").val().substr(-1);
    var genMsg_Recipient = $$("#genMsg_recipients").val();
    var genMsg_SenderID = $$("#genMsg_senderID").val();
    var formValid = true;
    if (genMsg_Msg === "" || genMsg_Recipient === "" || genMsg_SenderID === "") {
        formValid = false;
    }

    if (formValid) {
        Lungo.Router.article("form_pg2", "second_1");
        genMsg_Recipient = genMsg_Recipient.replace(/ /g, ',').replace(/[^0-9,]/g, '').replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
        genMsg_SenderID = genMsg_SenderID.replace(/[_\W]/g, '').slice(0, 11);

        var x = genMsg_Recipient.split(',');
        var lenX = x.length;

        while (lenX--) {
            if (x[lenX].startsWith('0')) {
                x[lenX] = "234" + x[lenX].slice(1);

            }
        }
        x = x.filter(function(a) {
            return (a.startsWith('234') || a.startsWith('0'));
        });
        $$('#genMsg_recipients').val(x.join(','))
        $$('#genMsg_senderID').val(genMsg_SenderID);
        $$('#genMsg_summary_msg').text(genMsg_Msg);
        $$('#genMsg_summary_recipientCount').text(x.length)
        $$('#genMsg_summary_msg_count').text(genMsg_msgCounter)
        $$('#genMsg_summary_SenderID').text(genMsg_SenderID)
        $$('#genMsg_summary_cost').text(parseInt(genMsg_msgCounter) * x.length)
    } else {
        Lungo.Notification.error('Error', 'Please fill all the required fields', '', 2);
    }
}

function gotogrupMsgpg2() {

    var grpMsg_Msg = $$("#grpMsg_msg").val();
    var grpMsg_msgCounter = $$("#grpMsg_msgCounter").val().substr(-1);
    var j = new Array();

    $$('#fetched_groups input[type="checkbox"]:checked').forEach(function(e) {
        j.push($$(e).val());
    });
    var grpMsg_Recipient = j.join(',');
    var grpMsg_SenderID = $$("#grpMsg_senderID").val();
    var formValid = true;
    if (grpMsg_Msg === "" || grpMsg_Recipient === "" || grpMsg_SenderID === "") {
        formValid = false;
    }

    if (formValid) {
        Lungo.Router.article("grp-messaging-summary", "grp-msg-summary-holder");
        //grpMsg_Recipient = grpMsg_Recipient.replace(/ /g, ',').replace(/[^0-9,]/g, '').replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',');
        grpMsg_SenderID = grpMsg_SenderID.replace(/[_\W]/g, '').slice(0, 11);


        $$('#grpMsg_recipient_hiddden').val(grpMsg_Recipient);
        $$('#grpMsg_senderID').val(grpMsg_SenderID);
        $$('#grpMsg_summary_msg').text(grpMsg_Msg);
        $$('#grpMsg_summary_recipientCount').text(j.length)
        $$('#grpMsg_summary_msg_count').text(grpMsg_msgCounter)
        $$('#grpMsg_summary_SenderID').text(grpMsg_SenderID)
        //$$('#grpMsg_summary_cost').text(parseInt(grpMsg_msgCounter) * x.length)
    } else {
        Lungo.Notification.error('Error', 'Please fill all the required fields', '', 2);
    }
}


function gototransferpg2() {
    var trnsf_email = $$("#trnsf_email").val();
    var trnsf_amt = parseInt($$("#trnsf_amount").val());
    var formValid = true;
    if (isNaN(trnsf_amt) || trnsf_email === "") {
        formValid = false;
    }

    if (formValid) {
        Lungo.Router.article("transfer-sms-summary", "transfer-sms-summary-holder");


        $$('#trnf_summary_recipient').text(trnsf_email);
        $$('#trnf_summary_amount').text(trnsf_amt)

    } else {
        var msg = 'Please fill all the required fields correctly';
        if (isNaN(trnsf_amt)) {
            msg = 'Please provide a valid amount';
        }
        Lungo.Notification.error('Error', msg, '', 2);
    }
}



$$("[data-action=back-to-form]").tap(function(event) {
    Lungo.Router.article("form", "form-normal");
});


