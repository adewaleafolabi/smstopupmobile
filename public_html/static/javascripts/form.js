$$("[data-action=send-gen-msg]").tap(function(event) {
    sendRegistration();
});

$$("[data-action=send-recharge-req]").tap(function(event) {
    if ($$('#recharge_pincode').val() === "") {
          Lungo.Notification.error('Error', "Please provide a valid pin", 'error', 2);
    } else {
        sendRechargeRequest();
    }
});


$$("[data-action=send-grp-msg]").tap(function(event) {
    sendGrpMsgReq();
});


$$("[data-action=send-transfer-req]").tap(function(event) {
    sendTrnsfRequest($$('#trnsf_amount').val(), $$('#trnsf_email').val());
});


$$("[data-action=bal-enq]").tap(function(event) {
    balEnq();
});



$$("[data-action=show-group-messaging]").tap(function(event) {
    Lungo.Router.article("grp-messaging", "grp-msg-holder");
    fetchGrup();

});