

Lungo.Events.init({
    'load section#summary': function(event) {
        loadSummary();
    },
    
    //mine
    'load section#form': function(event) {
        //Lungo.Notification.show('New Reg', 'Title', 2);
    },
    'touch article#form-normal button[data-action=goto-form-pg2]': function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        gotoformpg2();

    },
    
    'touch article#grp-msg-holder button[data-action=goto-grp-msg-pg2]': function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        gotogrupMsgpg2();

    },
    'touch article#transfer-sms-holder button[data-action=goto-transfer-pg2]': function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        gototransferpg2();

    },
    
    'touch article button[type="button"]': function(evt) {
        cleanUp($$(this).closest('article').attr('id'))
       // $$('article button[type="reset"]').tap(function(e){console.log($$(this).closest('article').attr('id'))})

    },
    
    'touch article#square-menu-items a[data-action=show-transfer-sms]': function() {
        Lungo.Router.article("transfer-sms", "transfer-sms-holder");
    },
    
    'touch article#square-menu-items a[data-action=show-cif]': function() {
        Lungo.Router.article("recharge", "recharge-holder");
    },
    'touch article#square-menu-items a[data-action=show-form]': function() {
        Lungo.Router.article("form", "form-normal");
    },
    'touch article#notification a[data-action=normal]': function() {
        Lungo.Notification.show('user', 'Title', 2);
    }


});

Lungo.ready(function() {



});
