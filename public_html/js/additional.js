String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) == str;
    };
}
function cleanUp(articleID) {
    $$('article#'+articleID+' input,textarea').forEach(function(e) {
        $$(e).val('');
    });
    $$('article#'+articleID+' input[type="checkbox"]:checked').forEach(function(e) {
        $$(e).attr('checked',false);
    });

}

function Count(field, counter) {

    var msgLength;


    if (field.value.length > 438)
    {
        msgLength = 3;
        alert('You have reached the limit for three text messages');
        field.value = field.value.substring(0, 438);
    }

    if (field.value.length <= 438)
    {
        msgLength = 3;
    }
    if (field.value.length <= 299)
    {
        msgLength = 2;
    }
    if (field.value.length <= 160)
    {
        msgLength = 1;
    }


    document.getElementById(counter).value = field.value.length + '/' + msgLength;
}